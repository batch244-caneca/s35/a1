// Activity:
// 1. Create a User schema.
const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.g9tzuwc.mongodb.net/batch244-new-signup?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => {
	console.log("We're connected to the database")
});

const userSchema = new mongoose.Schema({
	username : String,
	password : String
});

// 2. Create a User model.
const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// 3. Create a POST route that will access the "/signup" route that will create a user.

app.post("/signup", (req, res) => {
	User.findOne({ username : req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Username is already taken/registered");
		}else{
			if(req.body.username !== "" && req.body.password !== "" ){
				let newUser = new User({
				 	username: req.body.username,
				 	password: req.body.password
			});
			
				newUser.save((saveErr, savedUser) => {
					if(saveErr){
						return console.error(saveErr);
					}else{
						return res.status(201).send(`User ${req.body.username} has been successfully registered`);
					}
				})				
			} else {
				return res.send("BOTH username and password must be provided");
			}
		}
	})
})



// 4. Process a POST request at the "/signup" route using postman to register a user.
app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.error(err);
		} else {
			return res.status(200).json({data : result})
		}
	})
})

app.listen(port, () => {console.log(`Server running at port ${port}`)});